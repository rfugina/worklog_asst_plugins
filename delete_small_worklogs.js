registerPlugin(function(model) {
  var self = this;
  self.name = "DeleteSmallWorklogs";

  self.deleteSmallWorklog = function(worklog) {
    if(worklog.durationInSeconds < 30) {
      console.log("Deleting worklog",worklog.id);
      model.worklogManager.remove(worklog);
    }
  };

  self.unload = function() { 
    model.worklogManager.worklogCreated.disconnect(self.deleteSmallWorklog);
  }

  model.worklogManager.worklogCreated.connect(self.deleteSmallWorklog);
});
