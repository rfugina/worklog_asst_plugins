var indentLevel = 0;
function printObjectProperties(obj) {
  ++indentLevel;
  var indent = new Array(4*indentLevel+1).join(" ");
  var primitives = [];
  var objects = [];
  var value;
  var name;
  var ii;
  for(var property in obj) {
    if(obj.hasOwnProperty(property)) {
      value = obj[property];
      if(typeof(value) === "object") {
        objects.push([property,value]);
      } else {
        primitives.push([property,value]);
      }
    }
  }
  console.log(indent,"Primitives");
  for(ii = 0; ii < primitives.length; ++ii) {
    name = primitives[ii][0];
    value = primitives[ii][1];
    console.log(indent,name,value);
  }
  for(ii = 0; ii < objects.length && indentLevel <= 2; ++ii) {
    name = objects[ii][0];
    value = objects[ii][1];
    console.log(indent,name);
    printObjectProperties(value);
  }
  --indentLevel;
}

registerPlugin(function(model) {
  this.name = "Print API";
  printObjectProperties(model);
});
