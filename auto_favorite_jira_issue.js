//
// this code is from the WLS author by email/snippet
//
registerPlugin(function(model) {

  var self = this;
  self.name = "Auto favorite issue";

  self.worklogCreated = function(worklog) {
    if(!!worklog.task) worklog.task.favourite = true;
  };

  self.unload = function() {
    model.worklogManager.worklogCreated.disconnect(self.worklogCreated);
  };

  model.worklogManager.worklogCreated.connect(self.worklogCreated);

})

